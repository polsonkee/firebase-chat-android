# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/polson/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Setting for no obfuscation
#-dontobfuscate
#-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*,!code/allocation/variable

-keepattributes *Annotation*
-keep class no.flt.model.** { *; }

# Remove LogCat call
-assumenosideeffects class android.util.Log {
	public static *** i(...);
	public static *** d(...);
	public static *** v(...);
}

# Parse stuff
-keep class com.parse.** { *; }

# Crashlytics stuff
-keepattributes SourceFile,LineNumberTable
-keep class com.crashlytics.** { *; }

# Firebase stuff
-keep class com.firebase.** { *; }
-keep class org.apache.** { *; }
-keepnames class com.fasterxml.jackson.** { *; }
-keepnames class javax.servlet.** { *; }
-keepnames class org.ietf.jgss.** { *; }
-dontwarn org.w3c.dom.**
-dontwarn org.joda.time.**
-dontwarn org.shaded.apache.**
-dontwarn org.ietf.jgss.**

# Glide stuff
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
    **[] $VALUES;
    public *;
}

# Google Guava stuff
-dontwarn javax.annotation.**
-dontwarn javax.inject.**
-dontwarn sun.misc.Unsafe
-dontwarn com.google.common.collect.MinMaxPriorityQueue

# Butter Knife stuff
-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewBinder { *; }
-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}
-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}

# OkHttp stuff
-dontwarn okio.**

# Retrofit stuff
-dontwarn retrofit.appengine.UrlFetchClient

# Kotlin stuff
-dontwarn org.w3c.dom.events.**
