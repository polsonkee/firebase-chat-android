package no.flt.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import no.flt.R;
import rx.Observable;
import rx.android.view.OnClickEvent;
import rx.android.view.ViewObservable;
import rx.functions.Action1;
import rx.subjects.PublishSubject;

public class MessageBox extends FrameLayout {

    @Bind(R.id.message_edittext) EditText mMessageEditText;
    @Bind(R.id.message_send_button) Button mSendButton;

    private final PublishSubject<CharSequence> mMessageSubject = PublishSubject.create();


    public MessageBox(Context context) {
        super(context);
        initView();
    }

    public MessageBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public MessageBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MessageBox(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView();
    }

    private void initView() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_message_box, this);
        ButterKnife.bind(this);

        ViewObservable.clicks(mSendButton)
                .subscribe(new Action1<OnClickEvent>() {
                    @Override
                    public void call(OnClickEvent onClickEvent) {
                        CharSequence text = mMessageEditText.getText();
                        mMessageSubject.onNext(text);
                        mMessageEditText.setText("");
                    }
                });
    }

    public Observable<CharSequence> message() {
        return mMessageSubject;
    }
}
