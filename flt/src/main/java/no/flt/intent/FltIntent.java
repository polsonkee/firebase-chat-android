package no.flt.intent;

import android.content.Context;
import android.content.Intent;

import no.flt.BuildConfig;

public class FltIntent extends Intent {

    public static final String ACTION = BuildConfig.APPLICATION_ID + ".intent.action.";
    public static final String EXTRA = BuildConfig.APPLICATION_ID + ".intent.extra.";


    FltIntent(String action) {
        super(action);
    }

    FltIntent(Context context, Class<?> cls) {
        super(context, cls);
    }
}
