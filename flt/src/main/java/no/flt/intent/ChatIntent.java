package no.flt.intent;

import android.content.Context;

import no.flt.app.ChatActivity;

public class ChatIntent extends FltIntent {

    public ChatIntent(Context context) {
        super(context, ChatActivity.class);
    }
}
