package no.flt.app;

import android.os.Bundle;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.parse.ParseInstallation;

import butterknife.Bind;
import no.flt.R;
import no.flt.api.firebase.FltApi;
import no.flt.api.firebase.SimpleChildEventListener;
import no.flt.model.Message;
import no.flt.widget.MessageBox;
import rx.functions.Action1;
import timber.log.Timber;

public class ChatActivity extends FltActivity {

    @Bind(R.id.scrollview) ScrollView mScrollView;
    @Bind(R.id.textview) TextView mTextView;
    @Bind(R.id.message_box) MessageBox mMessageBox;

    private Firebase mUsers;
    private Firebase mMessages;


    @Override
    protected void onCreate(Bundle savedState) {
        super.onCreate(savedState);

        Firebase chat = new FltApi().allChat();
        mUsers = chat.child("users");
        mMessages = chat.child("messages");

        mMessageBox.message()
                .subscribe(new Action1<CharSequence>() {
                    @Override
                    public void call(CharSequence message) {
                        String id = ParseInstallation.getCurrentInstallation().getInstallationId();
                        Message m = new Message(id, String.valueOf(message));
                        mMessages.push().setValue(m);
                    }
                });

        mMessages.addChildEventListener(new SimpleChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot data, String previousChildName) {
                super.onChildAdded(data, previousChildName);
                try {
                    Message m = data.getValue(Message.class);
                    addText(m);
                } catch (Exception e) {
                    Timber.e(e, e.getMessage());
                }
            }
        });
    }

    private void addText(Message message) {
        mTextView.append(message.getSenderId().substring(0,8) + ":  " + message.getText() + "\n");
        mScrollView.post(new Runnable() {
            @Override
            public void run() {
                mScrollView.fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_chat;
    }
}
