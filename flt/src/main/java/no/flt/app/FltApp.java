package no.flt.app;

import android.app.Application;
import android.os.Build;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.firebase.client.Firebase;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

import io.fabric.sdk.android.Fabric;
import no.flt.BuildConfig;
import no.flt.R;
import no.flt.api.parse.ParseInstallationKey;
import no.flt.util.CrashlyticsTree;
import timber.log.Timber;

public class FltApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        initFabric();

        initTimber();
        initParse();
        initFirebase();
        initActivityLifecycleCallbacks();
    }

    private void initFabric() {
        Fabric.with(this, new Crashlytics());
    }

    private void initTimber() {
        Timber.plant(new CrashlyticsTree());
    }

    private void initParse() {
        Parse.setLogLevel(BuildConfig.DEBUG ? Log.INFO : Integer.MAX_VALUE);
        Parse.enableLocalDatastore(this);
        //ParseObject.registerSubclass(X.class);
        Parse.initialize(this, getString(R.string.parse_app_id), getString(R.string.parse_client_key));
        ParseInstallation install = ParseInstallation.getCurrentInstallation();
        install.put(ParseInstallationKey.BUILD_ID, Build.ID);
        install.put(ParseInstallationKey.BUILD_MODEL, Build.MODEL);
        install.put(ParseInstallationKey.BUILD_PRODUCT, Build.PRODUCT);
        install.put(ParseInstallationKey.BUILD_SDK, Build.VERSION.SDK_INT);

        ParseUser.enableAutomaticUser();
        ParseUser user = ParseUser.getCurrentUser();
        user.increment("RunCount");
        user.saveInBackground();

        install.put(ParseInstallationKey.USER, user);
        install.saveEventually();
    }

    private void initFirebase() {
        Firebase.setAndroidContext(this);
        Firebase.getDefaultConfig().setPersistenceEnabled(true);
    }

    private void initActivityLifecycleCallbacks() {
    }
}
