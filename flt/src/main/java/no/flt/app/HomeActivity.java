package no.flt.app;

import android.os.Bundle;

import no.flt.intent.ChatIntent;

public class HomeActivity extends FltActivity {

    @Override
    protected void onCreate(Bundle savedState) {
        super.onCreate(savedState);

        startActivity(new ChatIntent(this));
        finish();
    }

    @Override
    protected int getActivityLayout() {
        return 0;
    }
}
