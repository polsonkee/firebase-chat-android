package no.flt.receiver;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.parse.ParsePushBroadcastReceiver;

import no.flt.app.HomeActivity;
import timber.log.Timber;

public class ParsePushReceiver extends ParsePushBroadcastReceiver {

    @Override
    protected Class<? extends Activity> getActivity(Context context, Intent intent) {
        Class<? extends Activity> cls = super.getActivity(context, intent);
        if (cls == null) {
            cls = HomeActivity.class;
            Timber.e("ParsePushReceiver.getActivity() returns null. Default to: " + cls);
        }
        return cls;
    }
}
