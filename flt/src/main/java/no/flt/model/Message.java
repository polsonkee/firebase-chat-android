package no.flt.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.firebase.client.ServerValue;

public class Message {

    @JsonProperty("senderId") String mSenderId;
    @JsonProperty("text") String mText;
    @JsonProperty("timestamp") Object mTimestamp = ServerValue.TIMESTAMP;


    public Message() {}

    public Message(String senderId, String text) {
        mSenderId = senderId;
        mText = text;
    }

    public String getSenderId() {
        return mSenderId;
    }

    public String getText() {
        return mText;
    }

    protected Object getTimestamp() {
        return mTimestamp;
    }

    public Long getTime() {
        return (mTimestamp instanceof Long) ? (Long) mTimestamp : null;
    }
}
