package no.flt.api.parse;

public interface ParseInstallationKey {

    String USER = "user";

    String BUILD_ID = "deviceBuildId";
    String BUILD_MODEL = "deviceModel";
    String BUILD_PRODUCT = "deviceProduct";
    String BUILD_SDK = "osVersion";
}
