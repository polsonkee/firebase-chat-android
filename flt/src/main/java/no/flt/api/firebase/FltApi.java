package no.flt.api.firebase;

import com.firebase.client.Firebase;

public class FltApi {

    public static final String BASE_URL = "https://flt.firebaseio.com/";

    private static final String NODE_CHAT = "chats";
    private static final String NODE_CHAT_ALL = "all";


    private Firebase pleaseReplaceWithDagger() {
        return new Firebase(BASE_URL);
    }

    public Firebase allChat() {
        Firebase node = pleaseReplaceWithDagger().child(NODE_CHAT).child(NODE_CHAT_ALL);
        node.keepSynced(true);
        return node;
    }
}
