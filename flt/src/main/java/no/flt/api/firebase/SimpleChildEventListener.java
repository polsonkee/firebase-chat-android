package no.flt.api.firebase;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;

import timber.log.Timber;

public class SimpleChildEventListener implements ChildEventListener {

    @Override
    public void onChildAdded(DataSnapshot data, String previousChildName) {
        Timber.i("onChildAdded: " + data + " previousChildName: " + previousChildName);
    }

    @Override
    public void onChildChanged(DataSnapshot data, String previousChildName) {
        Timber.i("onChildChanged: " + data + " previousChildName: " + previousChildName);
    }

    @Override
    public void onChildRemoved(DataSnapshot data) {
        Timber.i("onChildRemoved: " + data);
    }

    @Override
    public void onChildMoved(DataSnapshot data, String previousChildName) {
        Timber.i("onChildMoved: " + data + " previousChildName: " + previousChildName);
    }

    @Override
    public void onCancelled(FirebaseError error) {
        Timber.w("onCancelled: " + error);
    }
}
